import os, shutil
import sys
import time
import re

# Importation des functions définies
from .P7_functions import *

print('PYTHON Version: ' + sys.version)
base_dir = os.getcwd()
#print("Working directory:", base_dir)

# Data manipulation
import numpy as np
print("NUMPY version :", np.__version__)
import pandas as pd
print("PANDAS version :", pd.__version__)

# Visualizations
import matplotlib as mpl
import matplotlib.pyplot as plt
print("MATPLOTLIB version :", mpl.__version__)
plt.style.use('ggplot')
%matplotlib inline

import seaborn as sns
print("SEABORN version :", sns.__version__)

import plotly
print("PLOTLY version :", plotly.__version__)
import plotly.express as px
import plotly.graph_objects as go

import scipy
print("SCIPY version :", scipy.__version__)

import streamlit as st
#st.write(st.__version__)
print("STREAMLIT version :", st.__version__)

# Garbage collector
import gc

import warnings
warnings.simplefilter(action='ignore') #, category=FutureWarning)

from sklearn.metrics import roc_auc_score, roc_curve
from sklearn.metrics import accuracy_score, precision_score, f1_score, recall_score
from sklearn.model_selection import KFold, StratifiedKFold

from contextlib import contextmanager   # contextlib2
from lightgbm import LGBMClassifier

from lightgbm import LGBMModel


# Constantes
RAND = 42
Nlignes = 2000


# LightGBM GBDT with KFold or Stratified KFold
# Parameters from Tilii kernel: https://www.kaggle.com/tilii7/olivier-lightgbm-parameters-by-bayesian-opt/code
def kfold_lightgbm(df, num_folds, stratified=False, debug=False):
    # Divide in training/validation and test data
    train_df = df[df['TARGET'].notnull()]
    test_df = df[df['TARGET'].isnull()]
    # Replace special characters
    train_df.columns = ["".join (c if c.isalnum() else "_" for c in str(x)) for x in train_df.columns]
    test_df.columns = ["".join (c if c.isalnum() else "_" for c in str(x)) for x in test_df.columns]
    # else
    #train_df = train_df.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))
    #test_df = test_df.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

    print("Starting LightGBM. Train shape: {}, test shape: {}".format(train_df.shape, test_df.shape))
    del df
    gc.collect()
    # Cross validation model
    if stratified:
        folds = StratifiedKFold(n_splits= num_folds, shuffle=True, random_state=RAND)
    else:
        folds = KFold(n_splits= num_folds, shuffle=True, random_state=RAND)
    # Create arrays and dataframes to store results
    oof_preds = np.zeros(train_df.shape[0])
    sub_preds = np.zeros(test_df.shape[0])
    feature_importance_df = pd.DataFrame()
    eval_results = dict()
    
    del_features = ['TARGET','SK_ID_CURR','SK_ID_BUREAU','SK_ID_PREV','index']
    feats = [f for f in train_df.columns if f not in del_features]    
    print("Number of features:", len(feats))
    
    #params = {'boosting': 'gbdt','objective': 'binary','metric': 'auc','learning_rate': 0.01,
    #    'num_leaves': 25, 'max_depth': 8, 'colsample_bytree': 0.3, 'seed': 101}
    # LightGBM parameters found by Bayesian optimization
    clf = LGBMClassifier(
    #clf = LGBMModel(
        #nthread=4,
        n_estimators=10000,
        learning_rate=0.02,
        num_leaves=34,
        colsample_bytree=0.9497036,
        subsample=0.8715623,
        max_depth=8,
        reg_alpha=0.041545473,
        reg_lambda=0.0735294,
        min_split_gain=0.0222415,
        min_child_weight=39.3259775,
        silent=-1,
        verbose=-1, )

    clf2 = LGBMClassifier(
        boosting_type = 'goss',
        #nthread=4,
        n_estimators=10000,
        learning_rate=0.005134,
        num_leaves=54,
        colsample_bytree=0.508716,
        subsample=1,
        max_depth=10,
        reg_alpha=0.436193,
        reg_lambda=0.479169,
        min_split_gain=0.024766,
        min_child_weight=40,
        silent=-1,
        verbose=-1,
        is_unbalance=False
        )
    
    for n_fold, (train_idx, valid_idx) in enumerate(folds.split(train_df[feats], train_df['TARGET'])):
        print("Fold:", n_fold+1, len(train_idx), len(valid_idx))
        train_x, train_y = train_df[feats].iloc[train_idx], train_df['TARGET'].iloc[train_idx]
        valid_x, valid_y = train_df[feats].iloc[valid_idx], train_df['TARGET'].iloc[valid_idx]
        
        _= clf.fit(train_x, train_y, eval_set=[(train_x, train_y), (valid_x, valid_y)], 
            eval_metric='auc', verbose=200, early_stopping_rounds=200)

        oof_preds[valid_idx] = clf.predict_proba(valid_x, num_iteration=clf.best_iteration_)[:, 1]
        sub_preds += clf.predict_proba(test_df[feats],
                                       num_iteration=clf.best_iteration_)[:, 1] / folds.n_splits

        fold_importance_df = pd.DataFrame()
        fold_importance_df["feature"] = feats
        fold_importance_df["gain"] = clf.booster_.feature_importance(importance_type='gain')
        fold_importance_df["split"] = clf.booster_.feature_importance(importance_type='split')
        fold_importance_df["importance"] = clf.feature_importances_
        fold_importance_df["fold"] = n_fold+1
        feature_importance_df = pd.concat([feature_importance_df, fold_importance_df], axis=0)
        eval_results['train_{}'.format(n_fold+1)] = clf.evals_result_['training']['auc']
        eval_results['valid_{}'.format(n_fold+1)] = clf.evals_result_['valid_1']['auc']
        
        print('Fold %2d AUC : %.6f' % (n_fold+1, roc_auc_score(valid_y, oof_preds[valid_idx])))
        #print('Fold %2d Accuracy : %.6f' % (n_fold+1, accuracy_score(valid_y, oof_preds[valid_idx])))
        del train_x, train_y, valid_x, valid_y   # , clf
        gc.collect()

    print('Full AUC score %.6f' % roc_auc_score(train_df['TARGET'], oof_preds))
    # Get the average feature importance between folds
    mean_importance_df = feature_importance_df.groupby('feature').mean().reset_index()
    print(mean_importance_df.head(4))
    mean_importance_df.sort_values(by='gain', ascending=False, inplace=True)
    
    print("Testing ....")
    print(feature_importance_df.head(1))
    print(mean_importance_df.head(1))
    display_importances(feature_importance_df, num=2, N_imp=20)
    
    # Generate (oof csv files ....)
    SUBMISSION_SUFIX = "_model_B"
    oof = pd.DataFrame()
    oof['SK_ID_CURR'] = train_df['SK_ID_CURR'].copy()
    train_df['PREDICTIONS'] = oof_preds.copy()
    test_df['PREDICTIONS'] = sub_preds.copy()
    #train_df['TARGET'] = train_df['TARGET'].copy()
    
    # Write submission file and plot feature importance
    if True:
    #if not debug:
        test_df['TARGET'] = 1 - sub_preds    # valeurs inversées
        predict = test_df[['SK_ID_CURR', 'TARGET']]
        predict.to_csv(submission_file_name, index= False)
    
    colonnes = ["SK_ID_CURR", "TARGET", "NEW_CREDIT_TO_ANNUITY_RATIO", "NEW_EXT_SOURCES_MEAN",
                "DAYS_BIRTH", "EXT_SOURCE_3", "EXT_SOURCE_2", "EXT_SOURCE_1"]
    train_df2 = train_df[colonnes].copy()
    train_df2.to_csv('./resultats/oof{}.csv'.format(SUBMISSION_SUFIX), index=False)
    test_df2 = test_df[colonnes].copy()
    test_df2.to_csv('./resultats/sub{}.csv'.format(SUBMISSION_SUFIX), index=False)
    
    # Save submission (test data) and feature importance
    test_df[['SK_ID_CURR', 'TARGET']].to_csv('./resultats/submission{}.csv'.format(SUBMISSION_SUFIX), index=False)
    mean_importance_df.to_csv('./resultats/feature_importance{}.csv'.format(SUBMISSION_SUFIX), index=False)
    del oof, oof_preds, train_df2, test_df2
    gc.collect()
    
    return (clf, sub_preds, predict, feature_importance_df, mean_importance_df)



import mlflow
print("MLFLOW version :", mlflow.__version__)
import lightgbm
print("LightGBM version :", lightgbm.__version__)

from mlflow.models.signature import infer_signature
import mlflow.sklearn
#import mlflow.lightgbm

import shutil

def sauve_mlflow(df, model, fichier):
    # Production du fichier MLFLOW
    train_df = df[df['TARGET'].notnull()]
    test_df = df[df['TARGET'].isnull()]
    # Replace special characters
    train_df.columns = ["".join (c if c.isalnum() else "_" for c in str(x)) for x in train_df.columns]
    test_df.columns = ["".join (c if c.isalnum() else "_" for c in str(x)) for x in test_df.columns]
    #
    feats = [f for f in train_df.columns if f not in ['TARGET','SK_ID_CURR','SK_ID_BUREAU','SK_ID_PREV','index']]    
    X_train, y_train = train_df[feats], train_df['TARGET']
    # Signature
    signature = infer_signature(X_train, y_train)
    # Sauvegarde du modele
    directory = os.path.abspath(fichier)
    if os.path.exists(directory):
        print("Removing directory:", directory)
        shutil.rmtree(directory)
    #mlflow.sklearn.save_model(model, fichier, signature=signature)
    mlflow.lightgbm.save_model(model.booster_, fichier, signature=signature)
    
    #lgbr = lightgbm.LGBMRegressor(num_estimators = 200, max_depth=5)
    #lgbr.fit(train[num_columns], train["prep_time_seconds"])
    #preds = lgbr.predict(predict[num_columns])
    #lgbr.booster_.save_model('lgbr_base.txt')
    #model = lightgbm.Booster(model_file='lgbr_base.txt')
    #model.predict(predict[num_columns])
    
# Deploy it in the project directory  ... in a Xterm
# command:  mlflow models serve -m fichier


def run_mlflow(df, modele, fichier):
    train_df = df[df['TARGET'].notnull()]
    test_df = df[df['TARGET'].isnull()]
    # Replace special characters
    train_df.columns = ["".join (c if c.isalnum() else "_" for c in str(x)) for x in train_df.columns]
    test_df.columns = ["".join (c if c.isalnum() else "_" for c in str(x)) for x in test_df.columns]
    
    feats = [f for f in train_df.columns if f not in ['TARGET','SK_ID_CURR','SK_ID_BUREAU','SK_ID_PREV','index']]    
    X_train, y_train = train_df[feats], train_df['TARGET']
    
    projet = base_dir + "/" + fichier
    print("Projet ...", projet)
    print("projet URI:", mlflow.set_tracking_uri(projet))
    try:
        print("Trying to create a projet ...")
        experienceID = mlflow.create_experiment(projet)
    except:
        print("Already running ...")
        experienceID = mlflow.set_experiment(projet)
    print("Experience_ID:", experienceID)
    artifact_path = fichier

    #with mlflow.start_run(experiment_id=experienceID) as run:
    with mlflow.start_run() as run:
        run_num = run.info.run_id
        model_uri = "runs:/{run_id}/{artifact_path}".format(
            run_id=run_num, artifact_path=artifact_path)
        print("Modele URI:", model_uri)

        model = modele

        t0 = time.time()
        model.fit(X_train, y_train)

        y_pred = model.predict(X_train)
        print("MLFLOW fitting done in:", time.time() - t0)
        y_df = pd.DataFrame(y_pred, index=X_train.index)
        y_df2 = pd.concat([y_train, y_df], axis=0)

        accura = accuracy_score(y_train, y_pred)
        roc_auc = roc_auc_score(y_train, y_pred)
        #mlflow.log_param()
        mlflow.log_metric('roc_auc', roc_auc)
        mlflow.lightgbm.log_model(model.booster_, artifact_path)
        #mlflow.register_model(model_uri=model_uri, name=artifact_path)
        
    return accura, roc_auc



def main(Nligne, debug=False):
    num_rows = Nlignes if debug else None
    #df = application_train_test(num_rows)
    df = application_train_test2(num_rows)
    with timer("Process bureau and bureau_balance"):
        bureau = bureau_and_balance(num_rows)
        print("Bureau df shape:", bureau.shape)
        df = df.join(bureau, how='left', on='SK_ID_CURR')
        del bureau
        gc.collect()
    with timer("Process previous_applications"):
        prev = previous_applications(num_rows)
        print("Previous applications df shape:", prev.shape)
        df = df.join(prev, how='left', on='SK_ID_CURR')
        del prev
        gc.collect()
    with timer("Process POS-CASH balance"):
        pos = pos_cash(num_rows)
        print("Pos-cash balance df shape:", pos.shape)
        df = df.join(pos, how='left', on='SK_ID_CURR')
        del pos
        gc.collect()
    with timer("Process installments payments"):
        ins = installments_payments(num_rows)
        print("Installments payments df shape:", ins.shape)
        df = df.join(ins, how='left', on='SK_ID_CURR')
        del ins
        gc.collect()
    with timer("Process credit card balance"):
        cc = credit_card_balance(num_rows)
        print("Credit card balance df shape:", cc.shape)
        df = df.join(cc, how='left', on='SK_ID_CURR')
        del cc
        gc.collect()
    #with timer("Process Bayesbian"):
    #    X = df.drop('TARGET', axis=1)
    #    y = df.TARGET
    #    opt_params = bayes_parameter_opt_lgb(X, y, init_round=5, opt_round=10,
    #                        n_folds=3, random_seed=RAND, n_estimators=100, learning_rate=0.05)
    print("Fichier df:", df.shape)
    with timer("Run LightGBM with kfold"):
        (pipeline, test_preds, test_df, feat_importance, mean_feat) = kfold_lightgbm(df, num_folds=10,
                                                    stratified=False, debug=debug)
    display(test_preds.shape, test_preds[:10])
    print('Min:', min(test_preds))
    print('Max:', max(test_preds))
    test_predit = np.sort(test_preds)[::-1]
    #print(test_predit[:10])
    
    print("Sauvegarde du modele en MLFLOW")
    file = 'LightGBMClassifier.mlflow'
    sauve_mlflow(df, pipeline, fichier=file)
    print("Running MLFLOW")
    file2 = 'LightGBMClassifier2.mlflow'
    accu, rocauc = run_mlflow(df, pipeline, fichier=file2)
    print("Accuracy:", accu)
    print("Roc_Auc:", rocauc)
    
    print("Running STREAMLIT")
    #streamlit_example(nn=100)
    
    return (test_preds, test_df, mean_feat)

if __name__ == "__main__":
    submission_file_name = "./resultats/sample_submission.csv"   # submission_kernel02
    with timer("Full model run"):
        predict, test_df, mean_feat = main(debug=True)
        #predict, test_df, mean_feat = main(debug=False)
        #np.sort(predict)[::-1][:25]
        test_df.head(1)
        mean_feat.head(2)
        #predict_df = pd.DataFrame(predict, columns=['y_pred'])
        #predict_df.describe().T
        #_= plt.hist(data=predict_df, x="y_pred", bins=50)
        #_= plt.show()
        test_df.describe().T
        _= plt.hist(data=test_df, x="TARGET", bins=50)
        nom_fichier2 = './resultats/lgbm_histo.png'
        _= plt.savefig(nom_fichier2)
        _= plt.show()

