# OpenClassRoom projet N°7
![img](images/logo_PretaDepenser.jpg)

## Dossier: OCR_projet7


## But:

1. Récupération d'un projet kaggle + nettoyage + EDA
2. Entrainer le modèle (fit + prédictions)
3. Création d'un Dashbord 


## Environnement  (bash ou zsh)

```

conda create env_X

conda activate env_X

pip install -r requirements.txt

jupyter notebook
```

## Publication sur BitBucket (GUI SourceTree) ou sur GitHub

```
git checkout branch
git status
git add .
git commit -m "message"
git push origin master (ou autres branches)
```

## Entrainement du modèle

1. Téléchargement des données 
2. Creation d'un notebook (nettoyage + fit du modèle (score) + prédictions)
3. Sauvegarde de fichiers intermédiaires
4. Sauvegarde du model fichier PICKLE (ou MLFLOW)


### Publication du modèle en local ou externe
(cf: README du sous-directory STREAMLIT_2)
